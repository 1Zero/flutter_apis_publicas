import 'package:apis_publicas_flutter/pages/api2.dart';
import 'package:flutter/material.dart';
import 'package:apis_publicas_flutter/pages/api1.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(        
        body: PageView(
          children: <Widget>[
            Myapi_public_1(),
            Myapi_public_2(),
          ],
            
        ),
      ),
    );
  }
}